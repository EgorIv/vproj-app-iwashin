from gino.ext.starlette import Gino
from .. import cofig
db=Gino(
    dsn=cofig.DB_DSN,
    pool_min_size=cofig.DB_POOL_MIN_SIZE,
    pool_max_size=cofig.DB_POOL_MAX_SIZE,
    echo=cofig.DB_ECHO,
    ssl=cofig.DB_SSL,
    use_connection_for_request=cofig.DB_USE_CONNECTION_FOR_REQUEST,
    retry_limit=cofig.DB_RETRY_LIMIT,
    retry_interval=cofig.DB_RETRY_INTERVAL
)

