from pydantic import BaseModel

class ClientsModel(BaseModel):
    id: int
    key_id: str