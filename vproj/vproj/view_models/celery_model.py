from pydantic import BaseModel


class Infra(BaseModel):
    count: int
    CPU: int
    memory: int

class CheckInfra(BaseModel):
    CPU: int
    memory: int

