from .clients import router
from .authorization import idp

def init_app(app):
    app.include_router(router)
    idp.add_swagger_config(app)