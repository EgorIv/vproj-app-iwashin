from fastapi import APIRouter
from ..view_models import celery_model
from ..view_models import clients
from ..celery_tasks import tasks

from ..models.clients import Clients
import json

router=APIRouter()

#@router.get("/clients/{uid}")
#async def get_user(uid: int):
#    client=await Clients.get_or_404(uid)
#    return client.to_dict()

@router.post("/infrastructure/new_infrastructure")
async def add_infrastructure(item: celery_model.Infra):
    item_json = item.dict()
    tasks.create_task.delay(item_json)
    return {"OK!"}

@router.get("/monitoring")
async def check_infrastructure():
    CPU= 5
    memory = 10
    return {'memory': memory, 'CPU': CPU}
