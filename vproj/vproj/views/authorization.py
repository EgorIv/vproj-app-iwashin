from fastapi import Depends, Query, Body, Header
from fastapi.responses import RedirectResponse
from .clients import router
from fastapi_keycloak import FastAPIKeycloak, OIDCUser, UsernamePassword, HTTPMethod, KeycloakUser
from pydantic import SecretStr
from typing import List, Optional
from ..models.clients import Clients


idp = FastAPIKeycloak(
    server_url="http://keycloak:8080/auth",
    client_id="login_app",
    client_secret="hWAN18m25BI7Th5uW4HnFntNVUEswguN",
    admin_client_secret="l8HhSx6iB2ifen9bwga2nhBuqvGBg5u5",
    realm="Vproj",
    callback_uri="http://localhost:8000/callback_auth"
)


@router.get("/")  # Unprotected
def root():
    return 'Hello World'

@router.post("/user/{user_id}/infrastructure", tags=["user-management"])
async def add_user_lable(user_id: str):
    client_rv=await Clients.create(key_id = user_id)
    return client_rv.to_dict()

# User Management

@router.get("/users", tags=["user-management"])
def get_users():
    return idp.get_all_users()

@router.post("/users", tags=["user-management"], status_code=201)
def create_user(first_name: str, last_name: str, email: str, password: SecretStr, id: str = None):
    return idp.create_user(first_name=first_name, last_name=last_name, username=email, email=email, password=password.get_secret_value(), id=id)


@router.get("/user/{user_id}", tags=["user-management"])
def get_user(user_id: str = None):
    return idp.get_user(user_id=user_id)


@router.put("/user", tags=["user-management"])
def update_user(user: KeycloakUser):
    return idp.update_user(user=user)


@router.delete("/user/{user_id}", tags=["user-management"])
def delete_user(user_id: str):
    return idp.delete_user(user_id=user_id)


@router.put("/user/{user_id}/change-password", tags=["user-management"])
def change_password(user_id: str, new_password: SecretStr):
    return idp.change_password(user_id=user_id, new_password=new_password)


@router.put("/user/{user_id}/send-email-verification", tags=["user-management"])
def send_email_verification(user_id: str):
    return idp.send_email_verification(user_id=user_id)


@router.get("/user")  # Requires logged in
def current_users(user: OIDCUser = Depends(idp.get_current_user())):
    return user


@router.get("/admin")  # Requires the admin role
def company_admin(user: OIDCUser = Depends(idp.get_current_user(required_roles=["Admin"]))):
    return f'Hi admin {user}'

@router.get("/login")
def login_redirect():
    return RedirectResponse(idp.login_uri)

@router.get("/callback_auth")
def callback_auth(session_state: str, code: str):
    access_token=idp.exchange_authorization_code(session_state=session_state, code=code)# This will return an access token
    return access_token


@router.get("/logout", tags=["auth-flow"])
def logout():
    return RedirectResponse(idp.logout_uri)

# Example User Requests

@router.get("/protected", tags=["example-user-request"])
def protected(user: OIDCUser = Depends(idp.get_current_user())):
    return user


@router.get("/current_user/roles", tags=["example-user-request"])
def get_current_users_roles(user: OIDCUser = Depends(idp.get_current_user())):
    return user.roles


@router.get("/admin", tags=["example-user-request"])
def company_admin(user: OIDCUser = Depends(idp.get_current_user(required_roles=["admin"]))):
    return f'Hi admin {user}'


@router.get("/login", tags=["example-user-request"])
def login(user: UsernamePassword = Depends()):
    return idp.user_login(username=user.username, password=user.password.get_secret_value())
