from celery import Celery
from celery.utils.log import get_task_logger
from .strategy import MonitoringResponse

import requests
import json
from kombu import Queue


celery_app = Celery('tasks', backend='rpc://', broker='amqp://user:password@rabbit:5672', )
celery_log = get_task_logger(__name__)


#trotler queue='Monitoring', rate_limit='20/m'


rate_limits = {
    'Monitoring': 1
    }

celery_app.conf.task_queues=[
    Queue('Monitoring'),
    Queue('Monitoring_token')
    ]

@celery_app.task
def token():
    return 1

@celery_app.on_after_configure.connect
def periodic_tasks(sender, **kwargs):
        sender.add_periodic_task(1.0, token.signature(queue='Monitoring_token'))

def rate_limit(self, task_group):
    with self.celery_app.connection_for_read() as conn:
        msg = conn.default_channel.basic_get(task_group+'_tokens', no_ack=True)
        if msg is None:
            self.retry(countdown=1)

@celery_app.task(bind=True, queue='Monitoring')# , rate_limit='20/m'
def create_task(self, req, max_retries=None):#
    rate_limit(self, 'Monitorng')
    result = json.loads(requests.get("http://localhost:8000/monitoring", params=None).text)
    strategy = MonitoringResponse(req, result)
    strategy.strategy.issuanse_infrastructure()
    celery_log.info(req)

