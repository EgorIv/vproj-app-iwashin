from abc import ABC, abstractmethod
from ..view_models import celery_model
import requests
import json


class MonitoringStrategy(ABC):
    def __init__(self,count , memory, CPU):
        self.memory=memory
        self.CPU=CPU
        self.count=count

    @abstractmethod
    def issuanse_infrastructure(self):
        pass


class CreateStrategy(MonitoringStrategy):
    def __init__(self,count, memory, CPU):
        super().__init__(count, memory, CPU)

    def issuanse_infrastructure(self):
        TOKEN = "6aea213aba5ebb6044d823b85df77a"
        ID = "36012506"
        data = {'count': self.count,'CPU' :self.CPU,'memory': self.memory}
        data_js = json.dump(data)
        f = requests.post(f"https://gitlab.com/api/v4/projects/{ID}/ref/main/trigger/pipeline?token={TOKEN}")
        print(f)
        print(f"Create {self.count} containers with {self.memory} and {self.CPU} CPU")


class ProvideStrategy(MonitoringStrategy):
    def __init__(self, count, memory, CPU):
        super().__init__(count, memory, CPU)

    def issuanse_infrastructure(self):
        host= f"Вот тут твои {self.count} контейнеров по {self.memory} памяти и {self.CPU} ЦПУ"
        print(host)
    

class StartStrategy(MonitoringStrategy):
    def __init__(self, count=4, memory=8, CPU=2):
        super().__init__(count, memory, CPU)

    def issuanse_infrastructure(self):
        print(f"Starting {self.count} containers with {self.memory} and {self.CPU} CPU")


class MonitoringResponse:
    def __init__(self, request: celery_model.Infra, response: celery_model.CheckInfra):
        self.response=response
        self.request=request
        CPU=request['CPU']*request['count']*1.2
        memory=request['memory']*request['count']*1.2 
        if response['CPU']!=0 and response['memory']!=0:
            if (CPU> response['CPU']) or (memory > response['memory']):
                self.strategy = CreateStrategy(request['count'], request['memory'], request['CPU'])
            else:
                self.strategy = ProvideStrategy(request['count'], request['memory'], request['CPU'])
        else:
            self.strategy = StartStrategy()
    
    
